php-horde-mongo (1.1.0-6) unstable; urgency=medium

  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 13:50:32 +0200

php-horde-mongo (1.1.0-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959341).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/copyright: Update copyright attributions.
  * d/rules: Remove source and doc files from installed library.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 21 May 2020 09:05:49 +0200

php-horde-mongo (1.1.0-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:38:01 +0200

php-horde-mongo (1.1.0-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 18:50:28 +0200

php-horde-mongo (1.1.0-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Fri, 06 Apr 2018 14:35:39 +0200

php-horde-mongo (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0

 -- Mathieu Parent <sathieu@debian.org>  Fri, 09 Sep 2016 15:32:52 +0200

php-horde-mongo (1.0.3-5) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 09:16:21 +0200

php-horde-mongo (1.0.3-4) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition

 -- Mathieu Parent <sathieu@debian.org>  Mon, 14 Mar 2016 09:09:06 +0100

php-horde-mongo (1.0.3-3) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 08:29:00 +0200

php-horde-mongo (1.0.3-2) unstable; urgency=medium

  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 02:06:49 +0200

php-horde-mongo (1.0.3-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 1.0.3

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 23:22:06 +0200

php-horde-mongo (1.0.2-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 22:50:13 +0200

php-horde-mongo (1.0.2-1) unstable; urgency=low

  * New upstream version 1.0.2

 -- Mathieu Parent <sathieu@debian.org>  Tue, 22 Oct 2013 17:27:20 +0200

php-horde-mongo (1.0.1-1) unstable; urgency=low

  * New upstream version 1.0.1

 -- Mathieu Parent <sathieu@debian.org>  Mon, 17 Jun 2013 22:07:18 +0200

php-horde-mongo (1.0.0-1) unstable; urgency=low

  * Use pristine-tar
  * New upstream version 1.0.0

 -- Mathieu Parent <sathieu@debian.org>  Thu, 06 Jun 2013 09:30:51 +0200

php-horde-mongo (1.0.0~beta1-1) unstable; urgency=low

  * Horde's Horde_Mongo package
  * Initial packaging (Closes: #709505)

 -- Mathieu Parent <sathieu@debian.org>  Thu, 23 May 2013 20:08:48 +0200
